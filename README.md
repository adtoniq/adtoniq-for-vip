# README #

Adtoniq for VIP is a slightly modified version of Adtoniq for WordPress, that includes modifications
to support the VIP platform. You can download the latest VIP plugin from here to install on a server.
You may need to work with your hosting provider (such as automattic) to ask them to install and
activate the plugin in your environment.

### Who do I talk to? ###

* support@adtoniq.com
* https://tech.adtoniq.com/contact-us/