<?php
/* Plugin Name: Adtoniq
Plugin URI: https://tech.adtoniq.com/
Description: Adtoniq for VIP helps publishers generate new advertising revenue with sustainable advertising for the ad blocked web.
Version: 1.0.0.11
Author: David Levine for Adtoniq
Author URI: http://tech.adtoniq.com/
License: GPLv2 or later
*/

static $adtoniq_version = "1.0.0.11";

function adtoniq_update_option($option_name, $new_value, $autoload) {
  wp_cache_delete($option_name, 'options');
  return update_option($option_name, $new_value, $autoload);
}

function adtoniq_delete_option($option_name) {
  wp_cache_delete($option_name, 'options');
  return delete_option($option_name);
}

function adtoniq_add_event($event) {
  if (function_exists('adtoniq_debug_write_event')) {
      adtoniq_debug_write_event($event);
  }
}

function adtoniq_add_event_var($event) {
  if (function_exists('adtoniq_debug_write_event_var')) {
      return adtoniq_debug_write_event_var($event);
  }
}

// For content protection
add_shortcode( 'adtoniq_protect', 'adtoniq_protect_shortcode' );
function adtoniq_protect_shortcode( $atts, $content = null ) {
   return '<div class="adtoniq_protect" style="display:none;">' . do_shortcode($content) . '</div>';
}

// Content displayed to all ad blocked users
add_shortcode( 'adtoniq_message_adblocked', 'adtoniq_message_adblocked_shortcode' );
function adtoniq_message_adblocked_shortcode( $atts, $content = null ) {
   return '<div class="adtoniq_adblocked" style="display:none;">' . do_shortcode($content) . '</div>';
}

// Content displayed to ad blocked users with acceptable ads disabled
add_shortcode( 'adtoniq_message_adblocked_noacceptable', 'adtoniq_message_adblocked_noacceptable_shortcode' );
function adtoniq_message_adblocked_noacceptable_shortcode( $atts, $content = null ) {
   return '<div class="adtoniq_adblocked_no_acceptable" style="display:none;">' . do_shortcode($content) . '</div>';
}

// Content displayed to blocked users with acceptable ads enabled
add_shortcode( 'adtoniq_message_adblocked_acceptable', 'adtoniq_message_adblocked_acceptable_shortcode' );
function adtoniq_message_adblocked_acceptable_shortcode( $atts, $content = null ) {
   return '<div class="adtoniq_acceptable" style="display:none;">' . do_shortcode($content) . '</div>';
}

// Content displayed to all non-blocked users
add_shortcode( 'adtoniq_message_nonblocked', 'adtoniq_message_nonblocked_shortcode' );
function adtoniq_message_nonblocked_shortcode( $atts, $content = null ) {
   return '<div class="adtoniq_nonblocked" style="display:none;">' . do_shortcode($content) . '</div>';
}

function adtoniq_adtoniq_delete_option_variables() {
  adtoniq_delete_option('adtoniq-state');
  adtoniq_delete_option('adtoniq-email');
  adtoniq_delete_option('adtoniq-name');
  adtoniq_delete_option('adtoniq-api-key');
  adtoniq_delete_option('adtoniq-password');
  adtoniq_delete_option('adtoniq-password2');
  adtoniq_delete_option('adtoniq-fqdn');
  adtoniq_delete_option('adtoniq-description');
  adtoniq_delete_option('adtoniq-head-injection');
  adtoniq_delete_option('adtoniq-quotaUsed');
  adtoniq_delete_option('adtoniq-quotaLimit');
  adtoniq_delete_option('adtoniq-quotaStart');
  adtoniq_delete_option('adtoniq-quotaEnd');
  adtoniq_delete_option('adtoniq-register-time');
  adtoniq_delete_option('adtoniq-is-private');
}

add_action('admin_menu', 'adtoniq_menu');

function adtoniq_menu() {
  add_menu_page('Adtonic', 'Adtonic', 'administrator', 'adtoniq-settings', 'adtoniq_settings_page', 'dashicons-admin-generic');
}

function adtoniq_vars_filter( $vars ){
  return $vars;
}
add_filter( 'query_vars', 'adtoniq_vars_filter' );

function adtoniq_get_server() {
  $adtoniq_server = get_option('adtoniq-debug-server', 'https://integration.adtoniq.com/');
  if (strlen($adtoniq_server) == 0)
    $adtoniq_server = 'https://integration.adtoniq.com/';
  return $adtoniq_server;
}

function adtoniq_is_public($apiKey, $fqdn) {
  $data = array(
    'operation'   => 'isPublic',
    'fqdn' => $fqdn,
    'apiKey' => $apiKey
    );
  $isPublic = adtoniq_post(adtoniq_get_server() . 'api/v1', $data);
  adtoniq_update_option('adtoniq-is-public', $isPublic, false);
  
  return $isPublic;
}

function adtoniq_register_page() {
  if ($_SERVER['REQUEST_METHOD'] === 'POST' && ! wp_verify_nonce( $_POST['register_adtoniq_field'], 'register_adtoniq' ) ) {
    print 'Sorry, your nonce did not verify.';
    adtoniq_add_event('Error: Nonce did not verify during registration');
    exit;
  }
  $actualServer = $_SERVER['SERVER_NAME'];
  $email = get_option('adtoniq-email');
  $name = get_option('adtoniq-name');
  $password = get_option('adtoniq-password');
  $password2 = get_option('adtoniq-password2');
  $fqdn = get_option('adtoniq-fqdn');
  $description = get_option('adtoniq-description');
  $adtoniq_server = adtoniq_get_server();
  $lastVersion = get_option('adtoniq-lastVersion');
  $error = "";
  $state = "";
  global $adtoniq_version;

  adtoniq_update_option('adtoniq-password', '', false);
  adtoniq_update_option('adtoniq-password2', '', false);

  if (strlen($email) > 0) {
    if (strlen($password) == 0)
      $error .= "Missing password. ";
    if (strlen($password2) == 0)
      $error .= "Missing second password. ";
    if (strlen($name) == 0)
      $error .= "Missing your name. ";
    if (strlen($description) == 0)
      $error .= "Missing web site description. ";
    if (strlen($password) > 0) {
      if (strlen($password) < 8)
        $error .= "Password must be at least 8 characters. ";
      if ($password != $password2)
        $error .= "Passwords do not match. ";
    }
    if (strlen($fqdn) == 0)
      $error .= "Missing Fully Qualified Domain Name. ";

    if (strlen($error) == 0) {
      $url = $adtoniq_server . 'api/v1';
      $data = array(
        'email' => $email,
        'operation' => "register",
        'name' => $name,
        'password' => $password,
        'fqdn' => $fqdn,
        'lastVersion' => $lastVersion,
        'description' => $description
        );
      $response = adtoniq_post($url, $data);
      if ($response == "emailwait") {
        $state = "emailwait";
        adtoniq_update_option('adtoniq-state', $state, true);
      } else if (substr($response, 0, 5) == "Error") {
        $error .= $response;
      } else if (strlen($response) > 0) {
        $state = "registered";
        adtoniq_update_option('adtoniq-register-time', time(), false);
        adtoniq_update_option('adtoniq-api-key', $response, false);
        adtoniq_update_option('adtoniq-state', $state, false);
        adtoniq_do_cache_update('');
        adtoniq_is_public($response, $fqdn);
      }
    }
  }
  if ("emailwait" == $state)
    adtoniq_email_wait_page();
  elseif ("registered" == $state)
    adtoniq_registered_page();
  elseif (strlen($state) == 0) {
    ?>
      <div class="adtoniq-plugin form" id="AdtoniqPlugin">
        <link rel="stylesheet" href="<?php echo plugins_url('css/adtoniq.css', __FILE__); ?>">
        <script src="<?php echo plugins_url('js/adtoniq-login.js', __FILE__); ?>"></script>
        <img
          class="adtoniq-logo"
          src='<?php echo plugins_url('images/cropped-Adtoniq-Logo-512w.png', __FILE__); ?>'
          alt="Adtoniq Logo" />

        <h2>Adtoniq Settings <small class="muted">version <?php echo $adtoniq_version; ?></small></h2>

        <p class="lead">Adtoniq helps you analyze and communicate with your ad blocked audience, recover lost revenue from ad blocked users, and protect the integrity of your web site from being broken by ad blockers. You need an Adtoniq account to use this plugin. Get a free account by filling out the form below and clicking Save Changes. Your free account is limited to 100,000 page views per month. <a href="https://tech.adtoniq.com/">View Adtoniq technical documentation</a>.
        </p>

        <?php if (strlen($error) > 0) { ?>
          <div class="alert alert-error">
            <h4>Apologies but something appears to have gone wrong:</h4>
              <ul>
              <?php
                $errors = explode(". ", $error);
                foreach ($errors as $err) {
                  if ($err != '') {
                    echo '<li>' . $err . '</li>';
                  }
                }
              ?>
              </ul>
          </div>
        <?php } ?>

        <form class="form-horizontal" method="post" action="options.php">
          <?php settings_fields( 'adtoniq-settings-group' ); ?>
          <?php do_settings_sections( 'adtoniq-settings-group' ); ?>
          <input type="hidden" name="adtoniq-lastVersion" value="<?php echo esc_attr( $adtoniq_version ); ?>" />
          <?php wp_nonce_field( 'register_adtoniq', 'register_adtoniq_field' ); ?>

          <div class="form-group">
            <label
              for="adtoniq-email"
              class="col-sm control-label">
              Your email address
            </label>
            <div class="col-md">
              <input
                type="email"
                class="form-control"
                onblur="AdtoniqLogin.validateEmail()"
                id="email"
                name="adtoniq-email"
                placeholder="name@corp.com"
                value="<?php echo esc_attr( $email ); ?>"
          tabindex="1"
                aria-describedby="emailHelpBlock">
                <span class="form-control-feedback" aria-hidden="true"></span>
            </div>
            <span id="emailHelpBlock" class="help-block col-sm-offset">Message Here</span>
          </div>

          <div class="form-group">
            <label
              for="adtoniq-name"
              class="col-sm control-label">
              Your name
            </label>
            <div class="col-md">
              <input
                type="text"
                class="form-control"
                onblur="AdtoniqLogin.validateName()"
                id="name"
                name="adtoniq-name"
                placeholder="First Last"
                value="<?php echo esc_attr( $name ); ?>"
          tabindex="2"
                aria-describedby="nameHelpBlock">
                <span class="form-control-feedback" aria-hidden="true"></span>
            </div>
            <span id="nameHelpBlock" class="help-block col-sm-offset">Message Here</span>
          </div>

          <div class="form-group">
            <label
              for="adtoniq-password"
              class="col-sm control-label">
              Password
            </label>
            <div class="col-md">
              <input
                type="password"
                class="form-control"
                onblur="AdtoniqLogin.validatePassword()"
                id="password"
                name="adtoniq-password"
                value="<?php echo esc_attr( $password ); ?>"
          tabindex="3"
                aria-describedby="pass1HelpBlock">
                <span class="form-control-feedback" aria-hidden="true"></span>
            </div>
            <span id="pass1HelpBlock" class="help-block col-sm-offset">Message Here</span>
          </div>

          <div class="form-group">
            <label
              for="adtoniq-password2"
              class="col-sm control-label">
              Type password again
            </label>
            <div class="col-md">
              <input
                type="password"
                class="form-control"
                onblur="AdtoniqLogin.validatePassword2()"
                id="password2"
                name="adtoniq-password2"
                value="<?php echo esc_attr( $password2 ); ?>"
          tabindex="4"
                aria-describedby="pass2HelpBlock">
                <span class="form-control-feedback" aria-hidden="true"></span>
            </div>
            <span id="pass2HelpBlock" class="help-block col-sm-offset">Message Here</span>
          </div>

          <div class="form-group">
            <label
              for="adtoniq-fqdn"
              class="col-sm control-label">
              Fully Qualified Domain Name
            </label>
            <div class="col-md">
              <input
                type="text"
                class="form-control"
                onblur="AdtoniqLogin.validateFQDN()"
                id="fqdn"
                name="adtoniq-fqdn"
                placeholder="<?php echo esc_attr( $actualServer ); ?>"
                value="<?php echo esc_attr( $fqdn ); ?>"
          tabindex="5"
                aria-describedby="fqdnHelpBlock">
                <span class="form-control-feedback" aria-hidden="true"></span>
            </div>
            <span id="fqdnHelpBlock" class="help-block col-sm-offset">Message Here</span>
          </div>

          <div class="form-group">
            <label
              for="adtoniq-description"
              class="col-sm control-label">
              Description of web site
            </label>
            <div class="col-md">
              <input
                type="text"
                class="form-control"
                onblur="AdtoniqLogin.validateDescription()"
                id="description"
                name="adtoniq-description"
                value="<?php echo esc_attr( $description ); ?>"
          tabindex="6"
                aria-describedby="descriptionHelpBlock">
                <span class="form-control-feedback" aria-hidden="true"></span>
            </div>
            <span id="descriptionHelpBlock" class="help-block col-sm-offset">Message Here</span>
          </div>

          <div class="form-group">
            <div class="col-sm-offset col-md">
              <?php /*submit_button();*/ ?>
              <input
                type="submit"
                name="submit"
                id="submit"
                class="btn btn-primary btn-block"
          tabindex="7"
                value="Save Changes">
            </div>
          </div>

          <div class="col-sm-offset col-md">
            <a href="https://www.adtoniq.com/terms-of-use/" tabindex=20 >View our terms of service</a>
          </div>
          <script>AdtoniqLogin.init();</script>
        </form>
      </div>
    <?php
  }
}

function adtoniq_check_for_email_confirmation() {
  $email = get_option('adtoniq-email');
  $name = get_option('adtoniq-name');
  $fqdn = get_option('adtoniq-fqdn');
  $adtoniq_server = adtoniq_get_server();

  $url = $adtoniq_server . 'api/v1';
  $data = array(
    'operation' => "checkEmailConfirmation",
    'email' => $email,
    'name' => $name,
    'fqdn' => $fqdn
    );
  $apiKey = adtoniq_post($url, $data);
  if (strlen($apiKey) > 0) {
    adtoniq_update_option('adtoniq-api-key', $apiKey, true);
    adtoniq_update_option('adtoniq-state', 'registered', true);
  }

  return $apiKey;
}

function adtoniq_email_wait_page() {
  global $adtoniq_version;
  $apiKey = adtoniq_check_for_email_confirmation();
  if (strlen($apiKey) == 0) {
    ?>

    <div class="adtoniq-plugin form">
      <link rel="stylesheet" href="<?php echo plugins_url('css/adtoniq.css', __FILE__); ?>">
      <img
        class="adtoniq-logo"
        src="<?php echo plugins_url('images/cropped-Adtoniq-Logo-512w.png', __FILE__); ?>"
        alt="Adtoniq Logo" />

      <div class="well">
        <h2>Adtoniq Settings</h2>
        <p class="lead">Check your email for your Adtoniq invitation. To protect the security of your account, you must accept this invitation to activate your account.  After you accept your invitation, click the Check For Email Activation button or refresh this page to continue.</p>

        <p class="lead">
          <input
            type="button"
            class="btn btn-secondary btn-block"
            value="Check For Email Activation"
            onclick='location.reload();' />
        </p>

        <p>To reset your Adtoniq WordPress plugin, cancel your registration and start the registration step again, click the Reset plugin and start again button below:</p>

        <form method="post" action="options.php">
          <?php settings_fields( 'adtoniq-settings-group' ); ?>
          <?php do_settings_sections( 'adtoniq-settings-group' ); ?>
          <input type="hidden" name="email" value=""/>
          <input type="hidden" name="name" value=""/>
          <input type="hidden" name="api_key" value=""/>
          <input type="hidden" name="password" value=""/>
          <input type="hidden" name="password2" value=""/>
          <input type="hidden" name="fqdn" value=""/>
          <input type="hidden" name="description" value=""/>
          <input type="hidden" name="head_injection" value=""/>
          <input type="hidden" name="state" value=""/>
          <input type="hidden" name="adtoniq-lastVersion" value="<?php echo esc_attr( $adtoniq_version ); ?>" />
          <input
            type="submit"
            name="submit"
            id="submit"
            class="btn btn-primary btn-block"
            value="Reset plugin and start again">
        </form>
      </div>
    </div>

<?php
  } else
    adtoniq_registered_page();
}

function adtoniq_registered_page() {
  $action = isset($_POST['adtoniqAction']) ? sanitize_text_field($_POST['adtoniqAction']) : '';
  $injectStr = get_option('adtoniq-head-injection', '');
  $lastUpdate = get_option('adtoniq-lastUpdate', '');
  $isPublic = get_option('adtoniq-is-public') == "true" ? true : false;

  switch ($action) {
    case "requestJSUpdate":
      $api_key = get_option('adtoniq-api-key');
      $passedAPIKey = isset($_POST['adtoniqAPIKey']) ? sanitize_text_field($_POST['adtoniqAPIKey']) : '';
      if ($passedAPIKey == $api_key)
        adtoniq_do_cache_update('');
      break;
    case "resetPlugin":
      adtoniq_add_event('Reset plugin');
      adtoniq_adtoniq_delete_option_variables();
      adtoniq_register_page();
      return;
      break;
  }

  $lastUpdate = get_option('adtoniq-lastUpdate', '');
  if (strlen($lastUpdate) == 0)
    $lastUpdate = "Updating pending... Refresh to see changes.";
  $email = get_option('adtoniq-email');
  $quotaUsed = get_option('adtoniq-quotaUsed');
  $quotaLimit = get_option('adtoniq-quotaLimit');
  $quotaStart = get_option('adtoniq-quotaStart');
  $quotaEnd = get_option('adtoniq-quotaEnd');
  $apiKey = get_option('adtoniq-api-key');
  $fqdn = get_option('adtoniq-fqdn');
  $actualServer = $_SERVER['SERVER_NAME'];
  $actualPort = $_SERVER['SERVER_PORT'];

  if (! $isPublic)
    $isPublic = adtoniq_is_public($apiKey, $fqdn) == "true" ? true : false;;
    
  if (strlen($injectStr) == 0)
    $injectStr = adtoniq_do_cache_update('');

  if (! ($actualPort == "80" || $actualPort == "443"))
    $actualServer .= ":" . $actualPort;

  $adtoniq_server = adtoniq_get_server();
  global $adtoniq_version;

  try {
    $url = $adtoniq_server . 'api/v1';
    $params = array(
      'operation' => "quota",
      'apiKey' => $apiKey,
      'fqdn' => $fqdn
    );
    $response = adtoniq_post($url, $params);
    if (strlen($response) > 0) {
      $quota = json_decode($response);
      if (! empty($quota)) {
        $quotaUsed = $quota->quotaUsed;
        $quotaLimit = $quota->quotaLimit;
        $quotaStart = $quota->quotaStart;
        $quotaEnd = $quota->quotaEnd;
        adtoniq_update_option('adtoniq-quotaUsed', $quotaUsed, true);
        adtoniq_update_option('adtoniq-quotaLimit', $quotaLimit, true);
        adtoniq_update_option('adtoniq-quotaStart', $quotaStart, true);
        adtoniq_update_option('adtoniq-quotaEnd', $quotaEnd, true);
      }
    }
  } catch(Exception $e) {
    ;
  }
  ?>

  <div class="adtoniq-plugin panel">
  <link rel="stylesheet" href="<?php echo plugins_url('css/adtoniq.css', __FILE__); ?>">
  <div class="adtoniq-logo-header">
  <a href="https://tech.adtoniq.com/">
  <img
  class="adtoniq-logo"
      src="<?php echo plugins_url('images/cropped-Adtoniq-Logo-512w.png', __FILE__); ?>"
          alt="Adtoniq Logo">
          <span><strong class="muted">Version <?php echo $adtoniq_version; ?></strong></span>
      </a>
    </div>
    <div class="well">
      <div class="analytics-panel">
        <iframe src="<?php echo $adtoniq_server; ?>adtoniqAnalytics.jsp?apikey=<?php echo $apiKey; ?>&fqdn=<?php echo $fqdn; ?>">
        </iframe>
      </div>

    <?php if (! $isPublic) { ?>
      <div class="alert alert-warning with-icon">
        <span class="icon-warning"></span>
        <h4>This server appears to be on a private network that is not publicly accessible.</h4>
        <p>As a result, this server will not receive automatic updates to ensure continued operation. <a href="https://tech.adtoniq.com/public-vs-private-servers/">Read more.</a></p>
      </div>
    <?php } ?>
      <div class="adtoniq-stats">
        <h2 class="page-header">This Adtoniq account is active and connected to Adtoniq Services.</h2>
        <div class="row">
          <div class="col-half">
            <p class="no-margin"><span class="fixed-width">Registered to: </span><?php echo $email; ?></p>
          <?php
            if (strlen($quotaLimit) == 0 || $quotaLimit == 0) { ?>
            <p class="no-margin"><strong>You are currently using the unlimited tier of service which gives you an unlimited number of page views.</strong></p>
          <?php } else { ?>
            <p class="no-margin"><span class="fixed-width">Quota Used:</span><?php echo number_format($quotaUsed); ?></p>
            <p class="no-margin"><span class="fixed-width">Quota Limit:</span><?php echo number_format($quotaLimit); ?></p>
            <p class="no-margin"><span class="fixed-width">Quota period start:</span><?php echo date("r", $quotaStart/1000); ?></p>
            <p class="no-margin"><span class="fixed-width">Quota period end:</span><?php echo date("r", $quotaEnd/1000); ?></p>
          <?php } ?>
            <p><a href="https://tech.adtoniq.com/shop2/">Learn more about Adtoniq quotas and pricing plans</a>.</p>
            <h4 class="no-margin">Features</h4>
            <p class="no-margin">TruBlock Analytics, JavaScript API, and WordPress shortcodes</p>
            <p class="no-margin"><a href="https://tech.adtoniq.com/">Learn more about how to use these features</a>.</p>
          </div>
          <div class="col-half">
            <h4 class="no-margin page-header">Javascript</h4>
            <p class="no-margin"><strong>Last update: </strong><?php echo $lastUpdate; ?></p>
            <p class="no-margin"><strong>JavaScript Size: </strong><?php echo number_format(strlen($injectStr)); ?> bytes</p>
            <p class="no-margin"><a href="https://tech.adtoniq.com/why-adtoniq-updates-your-javascript/">Learn more about updating your JavaScript</a>.</p>
            <div class="adtoniq-action">
              <form method="post">
                <?php wp_nonce_field( 'register_adtoniq', 'register_adtoniq_field' ); ?>
                <input type="hidden" name="adtoniqAction" value="requestJSUpdate">
                <input type="hidden" name="adtoniqAPIKey" value="<?php echo $apiKey; ?>">
                <input
                  type="submit"
                  class="btn btn-secondary btn-block"
                  name="updateJS"
                  onclick="return confirm('Are you sure you want to update your JavaScript now? This will ensure your server has the latest Adtoniq JavaScript available.');"
                  value="Update JavaScript now">
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="well">
        <p>To reset your Adtoniq WordPress plugin to its original state and register again, click the Reset plugin button below. This will immediately stop Adtoniq from collecting analytics, and your Adtoniq shortcodes and JavaScript API will stop functioning. This will <em>not</em> delete your Adtoniq account, nor will it delete any analytics in your account. It only clears the internal WordPress fields used to store your Adtoniq settings.</p>
        <p><strong>Important:</strong> To re-enable Adtoniq, you will need to re-register with the same email address (<?php echo $email; ?>) and password you used originally.</p>
        <form method="post">
          <?php wp_nonce_field( 'register_adtoniq', 'register_adtoniq_field' ); ?>
          <input type="hidden" name="adtoniqAction" value="resetPlugin"/>
          <input
            type="submit"
            class="btn btn-primary btn-block"
            name="resetPlugin"
            value="Reset Plugin" />
        </form>
      </div>
    </div>
  </div>
  <?php
}

function adtoniq_settings_page() {
  $state = get_option('adtoniq-state');

  if (strlen($state) == 0)
    adtoniq_register_page();
  elseif ("emailwait" == $state)
    adtoniq_email_wait_page();
  elseif ("registered" == $state)
    adtoniq_registered_page();
}

add_action( 'wp_head', 'adtoniq_head_injection' );

function adtoniq_head_injection() {
  adtoniq_update_cache();

  $state = get_option('adtoniq-state');

  if ("registered" == $state)
    echo adtoniq_generate_javascript();
}

function adtoniq_init() {
}
add_action( 'init', 'adtoniq_init');

function adtoniq_generate_javascript() {
  $injectStr = get_option('adtoniq-head-injection', '');

  $doubleClickId = get_option('adtoniq-dfp-id', '');
  $doubleClickSlot = get_option('adtoniq-dfp-slot', '');

  $adblockCheckerStr =
    "<iframe id='aq-ch' src='//static-42andpark-com.s3.amazonaws.com/html/danaton3.html?adname=" . $doubleClickSlot . "&adid=" . $doubleClickId .
    "' width='1' height='1' style='width:1px;height:1px;position:absolute;left:-1000;'></iframe>";

  $debug = "";
  if (function_exists('adtoniq_debug_head_injection'))
    $debug = adtoniq_debug_head_injection();
  return $adblockCheckerStr . $injectStr . adtoniq_gen_ga_script() . $debug;
}

function adtoniq_gen_ga_script() {
  $gaPropertyId = get_option('adtoniq-ga-property-id', '');
  $gaTrafficSplit = get_option('adtoniq-ga-traffic-split', '');
  $ga_bypass = get_option('adtoniq-ga-bypass', '');
  $script = "";
  if (strlen($gaPropertyId) > 0 && strlen($gaTrafficSplit) > 0) {
    $script = "<script>if (adtoniq && adtoniq.setGA) adtoniq.setGA('" . $gaPropertyId . "','" . $gaTrafficSplit . "','" . $ga_bypass . "');</script>";
  }
  return $script;
}

function adtoniq_do_cache_update($nonce) {
  $api_key = get_option('adtoniq-api-key');
  $injectStr = '';
  if (strlen($api_key) > 0) {
    $adtoniq_server = adtoniq_get_server();
    $fqdn = get_option('adtoniq-fqdn');
    $debugJS = get_option('adtoniq-debug-js', '');
    $email = get_option('adtoniq-email');
    $url = $adtoniq_server . 'api/v1';
    $version = get_option('adtoniq-lastVersion');
    $data = array('operation' => 'update', 'userName' => $email, 'apiKey' => $api_key, 'fqdn' => $fqdn, 'nonce' => $nonce, 'debug' => $debugJS, 'version' => 'VIP ' . $version);
    $response = adtoniq_post($url, $data);
    if (strlen($response) > 0) {
      $injectStr = $response;
      adtoniq_update_option('adtoniq-head-injection', $injectStr, true);
      adtoniq_update_option('adtoniq-lastUpdate', date('F d, Y h:i a T', time()), false);
    }
  }

  return $injectStr;
}

function adtoniq_update_cache() {
  $api_key = get_option('adtoniq-api-key');
  $passedAPIKey = isset($_POST['adtoniqAPIKey']) ? sanitize_text_field($_POST['adtoniqAPIKey']) : '';
  // Note: This is not a WordPress nonce. This nonce comes from the Adtoniq server and must be sent back for validation
  // The Adtoniq server validates this nonce, since it generated it. That is why we only check for length > 0.
  $nonce = isset($_POST['adtoniqNonce']) ? sanitize_text_field($_POST['adtoniqNonce']) : '';
  $validNonce = $api_key == $passedAPIKey && strlen($nonce) > 0;

  if ($validNonce) {
    adtoniq_do_cache_update($nonce);
    adtoniq_update_option('adtoniq-is-private', 'false', false);
  }
}

function adtoniq_post($url, $data) {
  $evt = '<b>POST url: ' . $url . '</b>';
  $evt .= '<div>' . adtoniq_add_event_var($data) . '</div>';

  $options = array(
  'http' => array(
    'header'  => "Content-type: application/x-www-form-urlencoded",
    'method'  => 'POST',
    'content' => http_build_query($data)
  )
  );
  $context  = stream_context_create($options);
  $response = trim(file_get_contents($url, false, $context));

  $evt .= '<div>' . 'response: ' . htmlspecialchars(substr($response, 0, 300)) . '</div>';
  adtoniq_add_event($evt);
  return $response;
}

add_action( 'admin_init', 'adtoniq_settings' );

function adtoniq_settings() {
  register_setting( 'adtoniq-settings-group', 'adtoniq-email' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-name' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-api-key' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-password' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-password2' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-fqdn' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-description' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-head-injection' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-state' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-lastUpdate' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-lastVersion' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-quotaUsed' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-quotaLimit' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-quotaStart' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-quotaEnd' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-register-time' );
  register_setting( 'adtoniq-settings-group', 'adtoniq-is-private' );
}