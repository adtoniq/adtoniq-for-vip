=== Adtoniq ===
Contributors: davidadtoniq, jboho
Tags: adblock, adblocker, ad blocker, ad block, ad blocking, advertisement, ads
Requires at least: 4.6
Tested up to: 4.7.3
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Get the most accurate ad block detection available and reach your blocked web.

== Description ==

Adtoniq is the leading platform for creating sustainable advertising for the blocked web.  The blocked web is becoming increasingly relevant in the digital advertising world.  Are you measuring your adblock rate on your site?  Would you like to [calculate the revenue potential](https://tech.adtoniq.com/how-much-is-your-blocked-web-worth/) that your blocked audience represents?
Adtoniq features the most accurate ad block measurement system in the world, an invaluable tool for WordPress admins, webmasters, and web developers.



Highlights:

= Accurate AdBlock Analytics =
Accurately measure your adblock rate without interference from adblockers and then explore different strategies to regain your revenue.  Once you install Adtoniq on your website, it records ad block analytics on every page view, except for those requests that come from robots that identify themselves as such, such as search engines like Google.  

= User-Friendly Short Codes =
WordPress shortcodes provide the building blocks so you can offer custom communications to your ad blocked audience.  

= JavaScript API =
Communicate with your ad blocked audience, offer them choices, and take actions.  Protect your content by requiring adblock users to whitelist your content.

= Easy-to-Interpret Dashboard =
Have monthly and real-time reports available at all times.  For every page view on your site whether or not the user has an ad blocker, the following information is securely transmitted from the user’s browser to Adtoniq: Number of ad units on the page, Timestamp of page view, Whether or not ads were blocked by an ad blocker, Whether or not the user has acceptable ads enabled, and Website and path of page view.

= Automated Testing =
Adtoniq is constantly monitoring adblockers and creating countermeasures to ensure the evolving efficiency of Adtoniq.

= On-Call Expert Customer Support and Consultation Services =
Rest assured that the Adtoniq team is available for support and consultation services.

== Installation ==

After installing and activating the Adtoniq plugin, you must register to create an Adtoniq analytics account in order to
get access to Adtoniq's analytics servers. All new Adtoniq accounts come with a free quota of 100,000 page views per month, and you
can purchase additional analytics server capacity as needed.

= Examples "How to Use Adtoniq" =

[Click here to view an example of how Adontiq works on your site.](https://tech.adtoniq.com/whitelist-or-micropayment-example/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin)

== Frequently Asked Questions ==

= What is Adtoniq? =
Adtoniq will increase your advertising revenue by helping you create sustainable advertising for the blocked web. Our first product is a WordPress plugin that you can install in a minute and then start measuring your [TruBlock™](https://tech.adtoniq.com/what-is-trublock/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin) rate. Once you establish your [TruBlock™](https://tech.adtoniq.com/what-is-trublock/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin) rate, you can move forward to design a strategy to monetize your ad blocked web. [Click here](https://tech.adtoniq.com/how-much-is-your-blocked-web-worth/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin) to learn more about the value of your blocked web.

= Who is using Adtoniq? =
Publishers large and small are currently using Adtoniq, but this information is currently confidential so we can not yet disclose which publishers are using Adtoniq. We “[eat our own dogfood](https://en.wikipedia.org/wiki/Eating_your_own_dog_food)” and use Adtoniq on our own websites. [See our live examples](https://tech.adtoniq.com/whitelist-or-micropayment-example/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin) to see Adtoniq in action.

= What exactly does Adtoniq do? =
Adtoniq supports you in earning new revenue! With Adtoniq you can accurately measure the number of ad blocked users you have, communicate directly with your ad blocked audience to offer them choices that increase your revenue, and protect content on your site from ad blocked users as an incentive to make a choice. All of this allows you to generate revenue from your blocked web. Read more about our [features](https://tech.adtoniq.com/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin). or [check out this example](https://tech.adtoniq.com/whitelist-or-micropayment-example/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin).

= Where do I get Adtoniq? =
You can [download Adtoniq](https://tech.adtoniq.com/download/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin) from our website for no charge and get free access to our servers for up to 100,000 page views per 30 days. We are working on getting into the WordPress marketplace and will publish the link to our page there once we are approved.

= How much does Adtoniq cost? =
There is no charge for using Adtoniq. However Adtoniq analytics servers have a monthly quota limit, which is free up until 100,000 page views
per month. You can purchase additional capacity by [signing up for an Adtoniq price plan](https://tech.adtoniq.com/pricing-plans/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin).

= How can I tell if Adtoniq is right for me? =
If you have a WordPress site and you’re making at least $1,000 per year in advertising, Adtoniq can generate new revenue for you by unleashing the potential in your blocked web. Follow our [five step guide](https://tech.adtoniq.com/how-to-test-adtoniq/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin) for evaluating Adtoniq, which leads you through estimating the value of your blocked web, and includes strategies on how to realize that revenue, along with working examples. Adtoniq also provides [ad block experts and consulting services](https://tech.adtoniq.com/ad-block-consulting-services/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin) to work with you to craft a customized strategy for monetizing your unique blocked web audience. [Contact us](https://tech.adtoniq.com/contact/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin) for more information about our consulting services.

= What is coming soon from Adtoniq? =
Coming soon, Adtoniq will allow you to enable any service that has been disabled by ad blockers, including ad servers, analytics systems, testing systems, measurement systems, data management tools, and anything else that gets blocked by ad blockers. [Contact us](https://tech.adtoniq.com/contact/?utm_source=wordpress_plugin_directory&utm_medium=plugin_directory&utm_campaign=wordpress_plugin) for information on our upcoming services, and whether you can participate in our early access program.

== Screenshots ==

1. View your rolling 30-day and realtime ad block rate
2. Register to get access to Adtoniq's TruBlock platform provided as a cloud-based service



