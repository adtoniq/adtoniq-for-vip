var AdtoniqLogin = (function () {
  'use strict';

  var module = {
    // elements (set in this.init)
    $email:        null,
    $name:         null,
    $password:     null,
    $password2:    null,
    $fqdn:         null,
    $description:  null,
    $button:       null,

    state: {
      email: '',
      name: '',
      password: '',
      password2: '',
      fqdn: '',
      description: ''
    },

    isValidEmail: function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },

    isNotEmpty: function (str) {
      if (typeof str !== 'string') return false;
      return str.length > 0;
    },

    isValidPassword: function (str) {
      if (typeof str !== 'string') return false;
      return str.length >= 8;
    },

    isMatching: function (str1, str2) {
      if (typeof str1 !== 'string' || typeof str2 !== 'string') return false;
      return str1 === str2;
    },

    isValidForm: function (formValues) {
      return (
        this.isNotEmpty(formValues.email) &&
        this.isValidEmail(formValues.email) &&
        this.isValidPassword(formValues.password) &&
        this.isValidPassword(formValues.password2) &&
        this.isNotEmpty(formValues.name) &&
        this.isNotEmpty(formValues.fqdn) &&
        this.isNotEmpty(formValues.description) &&
        this.isMatching(formValues.password, formValues.password2)
      );
    },

    setState: function () {
      this.state = {
        email: this.$email.value || '',
        name: this.$name.value || '',
        password: this.$password.value || '',
        password2: this.$password2.value || '',
        fqdn: this.$fqdn.value || '',
        description: this.$description.value || ''
      };
    },

    setButtonState: function () {
      var isValid = this.isValidForm(this.state);
      isValid ?
        this.$button.classList.remove('btn-disabled') : this.$button.classList.add('btn-disabled'); // jshint ignore:line
    },

    setValidationState: function (el, failureMessage, isWarning) {
      var helpBlockId = el.getAttribute('aria-describedby');
      var helpBlock = document.getElementById(helpBlockId);
      var inputContainer = el.parentElement.parentElement;
      var icon = el.nextElementSibling;

      if (failureMessage) {
        inputContainer.className = isWarning ?
          'form-group has-warning has-feedback' : 'form-group has-error has-feedback';
        icon.className = isWarning ?
          'form-control-feedback icon-warning' : 'form-control-feedback icon-error';
        helpBlock.innerHTML = failureMessage;
        helpBlock.style.display = 'block';
      } else {
        inputContainer.className = 'form-group has-success has-feedback';
        icon.className = 'form-control-feedback icon-ok';
        helpBlock.style.display = 'none';
      }
    },

    validateEmail: function () {
      this.render();
      this.setValidationState(
        this.$email,
        this.isValidEmail(this.state.email) ? '' :
          this.isNotEmpty(this.state.email) ? 'Invalid email' : 'Missing email'
      );
    },

    validateName: function () {
      this.render();
      this.setValidationState(
        this.$name,
        this.isNotEmpty(this.state.name) ? '' : 'Missing name'
      );
    },

    validatePassword: function () {
      this.render();
      this.setValidationState(
        this.$password,
        this.isValidPassword(this.state.password) ? '' : 'Must be at least 8 characters'
      );
      if (!!this.state.password2) {
        this.setValidationState(
          this.$password,
          this.isMatching(this.state.password, this.state.password2) ? '' : 'Passwords do not match'
        );
        this.setValidationState(
          this.$password2,
          this.isMatching(this.state.password, this.state.password2) ? '' : 'Passwords do not match'
        );
      }
    },

    validatePassword2: function () {
      this.render();
      if (this.isValidPassword(this.state.password) && this.isValidPassword(this.state.password2)) {
        this.setValidationState(
          this.$password,
          this.isMatching(this.state.password, this.state.password2) ? '' : 'Passwords do not match'
        );
        this.setValidationState(
          this.$password2,
          this.isMatching(this.state.password, this.state.password2) ? '' : 'Passwords do not match'
        );
      } else {
        this.setValidationState(
          this.$password2,
          this.isValidPassword(this.state.password2) ? '' : 'Must be at least 8 characters'
        );
      }
    },

    validateFQDN: function () {
      this.render();
      this.setValidationState(
        this.$fqdn,
        this.state.fqdn === this.$fqdn.placeholder ? '' : 'Should domain be ' + this.$fqdn.placeholder + '?',
        true
      );
    },

    validateDescription: function () {
      this.render();
      this.setValidationState(
        this.$description,
        this.isNotEmpty(this.state.name) ? '' : 'Missing web site description'
      );
    },

    forgotPassword: function () {
      var email = prompt('Enter your email address to send a password reset email to yourself:');
      if (this.isValidEmail(email)) {
        this.ajax('https://integration.adtoniq.com/' + 'api/v1', function(response) {
          alert(response);
        }, 'operation=requestPasswordReset&email=' + email);
      } else if (email && email.length > 0)
        alert(email + ' was not a valid email');
    },

    ajax: function (url, onDone, params) {
      this.ajax2(url, onDone, true, null, params);
    },

    ajax2: function (url, onDone, synch, onFail, params) {
      var xmlhttp = new XMLHttpRequest();
      if (onDone) {
        xmlhttp.onreadystatechange = function() {
          if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200)
              onDone(xmlhttp.responseText.trim());
            else if (onFail)
              onFail();
          }
        };
      }
      xmlhttp.open('POST', url, synch);
      xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xmlhttp.send(params);
    },

    render: function () {
      this.setState();
      this.setButtonState();
      console.log('state:', this.state);
    },

    init: function () {
      // assign elements
      this.$email        = document.getElementById('email');
      this.$name         = document.getElementById('name');
      this.$password     = document.getElementById('password');
      this.$password2    = document.getElementById('password2');
      this.$fqdn         = document.getElementById('fqdn');
      this.$description  = document.getElementById('description');
      this.$button       = document.getElementById('submit');
      // kick off the module
      this.render();

    // Because validation happens after the next element to tab to is calculated, this
    // hack prevents accidentally tabbing into the button when it is disabled. If we set the tabindex=-1 on the button,
    // it will get skipped over in the tab sequence when it's enabled so we can't use that to prevent being tabbed into.
    var that = this;
    this.$button.onfocus = function () {
    if (!that.isValidForm(that.state))
      that.$description.focus();
    };
    }
  };

  return module;

}());

if (typeof module !== 'undefined') {
  module.exports = AdtoniqLogin;
}
